import { Component } from "@angular/core";
import { ApiService } from "./services/api.service";
import { delay, Observable, Subject, switchMap, tap } from "rxjs";

@Component({
    selector: "app-root",
    templateUrl: "./app.component.html",
    styleUrls: ["./app.component.css"]
})
export class AppComponent {
    title = "streams";
    click: Subject<any> = new Subject();

    firstStream: Subject<number> = new Subject<number>();
    secondStream: Subject<number> = new Subject<number>();

    lastStream: Observable<number> = this.click.pipe(
        switchMap(() => this.apiService.get()),
        delay(1000),
        tap((response) => this.firstStream.next(response)),
        delay(1000),
        tap((response) => this.secondStream.next(response)),
        delay(1000)
    );

    constructor(readonly apiService: ApiService) {}

    onClick(): void {
        // @ts-ignore
        this.click.next();
    }
}
