import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";

@Injectable({
    providedIn: "root"
})
export class ApiService {
    url: string = "http://www.randomnumberapi.com/api/v1.0/random?min=100&max=1000&count=1";
    constructor(private readonly httpClient: HttpClient) {}

    get(url: string = this.url): Observable<any> {
        return this.httpClient.get(url);
    }
}
